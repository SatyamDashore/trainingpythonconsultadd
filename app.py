from flask import Flask,jsonify
from flask.wrappers import Request

from sqlalchemy import create_engine, MetaData, Table, Column, Integer, String
engine = create_engine('sqlite:///Income.db', echo = True)
meta = MetaData()

incomes = Table(
   'incomes', meta, 
   Column('id', Integer, primary_key = True), 
   Column('name', String), 
   Column('income', Integer),
)

meta.create_all(engine)   
conn = engine.connect()      

"""        CODE TO INSERT MULTIPLE VALUES INTO incomes TABLE """


"""
conn.execute(incomes.insert().values([
   {'name':'Sumeet','income':21000},
{'name':'Ritik','income':20000},
{'name':'Aman','income':23000},
]))
"""

# CRUD operations on runtime data

app = Flask(__name__)



@app.route("/")
def show():
    conn = engine.connect()
    result = conn.execute("SElECT * FROM incomes")
    #print(type(result))
    inc=[]
    for row in result:
        inc.append({'id':row[0],'name':row[1],'income':row[2]})
        print(row)
    return jsonify(inc)
    

@app.route("/add/<n>/<inc>")
def create(n,inc):
    conn=engine.connect()
#   conn.execute(incomes.insert().values([{'name':n,'income':inc}]))
    conn.execute("INSERT INTO incomes (name,income) VALUES('{}',{})".format(n,inc))
    print("Recorded Added!!!")
    return "Record Added!!!"


@app.route("/show/<n>")
def read(n):
    conn = engine.connect()
    result = conn.execute("SELECT * FROM incomes WHERE name = '{}'".format(n))
    if(result):
        inc=[]
        for row in result:
            inc.append({'id':row[0],'name':row[1],'income':row[2]})
            print(row)
        return jsonify(inc)
    else:
        return "Record Not Found!!!"


@app.route("/update/<n>/<un>/<uinc>")
def update(n,un,uinc):
    conn=engine.connect()
    conn.execute("UPDATE incomes SET name = '{}' , income = {} WHERE  name = '{}'".format(un,uinc,n))
    print("Record Updated!!!")
    return "Record Updated!!!"


@app.route("/delete/<n>")
def delete(n):
    conn=engine.connect() 
    conn.execute("DELETE FROM incomes WHERE name='{}'".format(n))
    return "Record Deleted!!!"      


if __name__=="__main__":
    app.run(debug=True)