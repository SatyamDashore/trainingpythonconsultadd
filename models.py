from app import db

class Employee(db.Model):
    emp_id = db.Column(db.Integer,primary_key= True)
    salary  = db.Column(db.Float,default=1000.0,nullable= True)
    name =  db.Column(db.String(120),nullable= False)


    def __repr__(self):
        return f'{self.name}'